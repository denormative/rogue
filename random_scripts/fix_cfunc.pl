#!/usr/bin/perl -w
use strict;
use File::Copy;

sub handleFile($) {
	my $file = shift;
	
	# backup
	copy("$file","$file.bak") or die "Copy failed: $!";
	open INF, "<", "$file.bak" or die $!;
	open OUTF, ">", "$file" or die $!;

	my @code = <INF>;

	my $numChanges = 0;
	my $lastline  = "";

	for (my $i = 0; $i < scalar(@code); $i++) {
		my $line = $code[$i];
		$line =~ s/\f//g;

		if (($lastline =~ /^([\w][^\n]*)$/ or $lastline =~ /^\s*$/
				   	or $lastline =~ /\*\/\s*$/ or $lastline =~ /^\}\s*$/)
			   	and $line =~ /^([\w][^\n]*\))\s*$/) {

			$numChanges++;
			
			if ($lastline =~ /\*\/\s*$/) {
				# if the last line happens to be a comment and the current line is a function,
				# we're obviously a function with a default int return value, so write out the
				# line and then pretend we've got an empty line
				print OUTF $lastline;
				$lastline = "";
			} elsif ($lastline =~ /^\}\s*$/) {
				# as above, except previous line is /^}$/
				print OUTF $lastline;
				$lastline = "\n";
			} elsif ($lastline =~ /^\s*$/) {
				$lastline = "\n";
			} else {
				# just cleanup the normal line a little and strip off the \n
				$lastline =~ s/^([\w][^\n]*)\n$/$1/;
			}
#			if(substr($lastline, -1, 1) eq "*") {
#				print ">> $lastline$line";
#			} else {
#				print ">> $lastline $line";
#			}
			my @params;
			$i++;
			while (not $code[$i] =~ /^{$/) {
				push @params, $code[$i];
				$i++;
			}
#			foreach my $p (@params) {
#				print ">>> $p";
#			}
			my $func_name = ( $line =~ /([^(]+)/ )[0];
			$func_name =~ s/(.*?)[\s]*$/$1/; # strip off any spaces at the end
#			print ">>>> $func_name\n";
			my @func_param_names = split /,\s*/, ( $line =~ /[^(]+\((.*)\)/g )[0];
#			foreach my $p (@func_param_names) {
#				print ">>>> $p\n";
#			}
			my @func_param_names_new;
			foreach my $pn (@func_param_names) {
				my $added = 0;
				foreach my $p (@params) {
					# strip any comments
					$p =~ s/(.*)\/\*.*\*\/(.*)/$1$2/;
					# uniq-afy spaces
					$p =~ s/([\s]{2,}|[\t]+)/ /g;

					if ($p =~ /\W${pn}\W/) {
						my $newparam = $p;
						$newparam =~ s/^([^,;]+).*$/$1/;
					   	$newparam =~ s/^(.+)(?<!\w)\w+$/$1/ if not $newparam =~ /[^\w](func|dfunc)[^\w]/;
						chomp $newparam;
#						print ">>>>> $pn: $newparam\n";

#					   	my $newparamname  = $pn;
#						$newparamname =~ s/^.*[, ]([^, ]*$pn[^, ]*)[,;].*$/$1/;
#						print ">>>>> $pn-$p: $newparamname\n";
						if ($pn ne "func") {
							push @func_param_names_new, "$newparam$pn";
							$added = 1;
						} else {
							#$newparam =~ s/^([^;+)[;]*$/$1/;
							push @func_param_names_new, "$newparam";
							$added = 1;
						}
					}
				}
				if ($added == 0) {
					if ($pn =~ /\s/) {
						push @func_param_names_new, "$pn";
					} elsif ($pn =~ /\.\.\./) {
						push @func_param_names_new, "$pn";
					} else {
						push @func_param_names_new, "int $pn";
					}
				}
			}

			if($func_name =~ /\s/) {
				print OUTF "$func_name(";
			} elsif($lastline =~ /^\s*$/) {
				print OUTF "${lastline}int $func_name(";
			} elsif(substr($lastline, -1, 1) eq "*") {
				print OUTF "$lastline$func_name(";
			} else {
				print OUTF "$lastline $func_name(";
			}
			print OUTF join(', ', @func_param_names_new).") {\n";
			print OUTF "//DEL $lastline";
			print OUTF "\n" if not ($lastline =~ /^\s*$/ and substr($lastline, -1, 1) eq "\n");
			print OUTF "//DEL $line";
			foreach my $p (@params) {
				print OUTF "//DEL $p";
				if(not substr($p, -1, 1) eq "\n") {
					print OUTF "\n";
				}
			}
			# HACK

			# wipe out the current line so that we don't run around printing it out
			$line = "";
		} else {
			print OUTF $lastline;
		}
		$lastline = $line;
	}
	# flush the last line to output before closing
	print OUTF $lastline;
	close OUTF;

	return $numChanges;
}

sub handleDir($) {
	my $dir = shift;

	my @files = <$dir/*.c>;

	foreach my $file (@files) {
		print "> $file\n";
		my $numChanges = handleFile($file);
		print "\t$numChanges changes\n" if $numChanges > 0;
	}
}

foreach (@ARGV) {
	handleDir($_);
}

