set(CMAKE_CXX_FLAGS ${ROGUE_FLAGS})

include_directories(.)
file(GLOB UROGUESRC *.c)
file(GLOB UROGUEINC *.h)

set_source_files_properties(
    ${UROGUESRC} ${UROGUESRC} ${VARIANTSRC_A}
    PROPERTIES LANGUAGE ${ROGUE_LANG} )

add_executable (urogue
    ${UROGUESRC}  ${UROGUEINC}
    ${PDCURSESRC}   ${PDCURSEINC}
    ${ROGUEUNIFYSRC}  ${ROGUEUNIFYINC}
    ${VARIANTSRC_A} ${VARIANTINC_A}
    )

if(CURSES_FOUND)
    target_link_libraries (urogue ${CURSES_LIBRARIES})
endif()
