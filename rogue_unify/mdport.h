/*
    mdport.h - Machine Dependent Code for Porting Unix/Curses games

    Copyright (C) 2008 Nicholas J. Kisseberth
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name(s) of the author(s) nor the names of other contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR(S) OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.
*/

#ifndef MDPORT_H
#define MDPORT_H

#include <stdio.h>

#ifdef HAVE_CONFIG_H
#ifdef PDCURSES
#undef HAVE_UNISTD_H
#undef HAVE_LIMITS_H
#undef HAVE_MEMORY_H
#undef HAVE_STRING_H
#endif
#include "config.h"

#elif defined(__DJGPP__)
#define HAVE_SYS_TYPES_H 1
#define HAVE_PROCESS_H 1
#define HAVE_PWD_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_SETGID 1
#define HAVE_GETGID 1
#define HAVE_SETUID 1
#define HAVE_GETUID 1
#define HAVE_GETPASS 1
#define HAVE_SPAWNL 1
#define HAVE_ALARM 1
#define HAVE_ERASECHAR 1
#define HAVE_KILLCHAR 1
#define HAVE_CRYPT

#elif defined(_WIN32)
#define HAVE_CURSES_H
#define HAVE_TERM_H
#define HAVE__SPAWNL
#define HAVE_SYS_TYPES_H
#define HAVE_PROCESS_H
#define HAVE_ERASECHAR 1
#define HAVE_KILLCHAR 1
#ifndef uid_t
typedef unsigned int uid_t;
#endif
#ifndef pid_t
#ifndef __MINGW32__
typedef unsigned int pid_t;
#endif
#endif
#elif defined(__CYGWIN__)
#define HAVE_SYS_TYPES_H 1
#define HAVE_PWD_H 1
#define HAVE_PWD_H 1
#define HAVE_SYS_UTSNAME_H 1
#define HAVE_ARPA_INET_H 1
#define HAVE_UNISTD_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_NCURSES_TERM_H 1
#define HAVE_ESCDELAY
#define HAVE_SETGID 1
#define HAVE_GETGID 1
#define HAVE_SETUID 1
#define HAVE_GETUID 1
#define HAVE_GETPASS 1
#define HAVE_GETPWUID 1
#define HAVE_WORKING_FORK 1
#define HAVE_ALARM 1
#define HAVE_SPAWNL 1
#define HAVE__SPAWNL 1
#define HAVE_ERASECHAR 1
#define HAVE_KILLCHAR 1
#define HAVE_CRYPT 1

#else /* standards based unix */
#define HAVE_SYS_TYPES_H 1
#define HAVE_PWD_H 1
#define HAVE_SYS_UTSNAME_H 1
#define HAVE_ARPA_INET_H 1
#define HAVE_UNISTD_H 1
#define HAVE_CRYPT_H 1
#define HAVE_LIMITS_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_UTMPX_H 1
#define HAVE_ERRNO_H 1
#define HAVE_TERM_H 1
#define HAVE_SETGID 1
#define HAVE_GETGID 1
#define HAVE_SETUID 1
#define HAVE_GETUID 1
#define HAVE_SETREUID 1
#define HAVE_SETREGID 1
#define HAVE_CRYPT 1
#define HAVE_GETPASS 1
#define HAVE_GETPWUID 1
#define HAVE_WORKING_FORK 1
#define HAVE_ERASECHAR 1
#define HAVE_KILLCHAR 1
#ifndef _AIX
#define HAVE_GETLOADAVG 1
#endif
#define HAVE_ALARM 1
#endif

#ifdef __DJGPP__
#undef HAVE_GETPWUID /* DJGPP's limited version doesn't even work as documented */
#endif

#ifdef __MINGW32__
#define HAVE_ERRNO_H
#endif

#define MD_STRIP_CTRL_KEYPAD 1

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#if !defined(PATH_MAX) && defined(_MAX_PATH)
#define PATH_MAX _MAX_PATH
#endif

#if !defined(PATH_MAX) && defined(_PATH_MAX)
#define PATH_MAX _PATH_MAX
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

/*#ifndef HAVE_CRYPT
char *	crypt(const char *key, const char *setting);
#else
#ifdef HAVE_CRYPT_H
#include <crypt.h>
#endif
#endif*/

/*int	md_issymlink(char *sp);
*/

void	md_init(int options);
void	md_onsignal_default(void);
void	md_onsignal_exit(void);
void	md_onsignal_autosave(void);
int		md_hasclreol(void);
void	md_putchar(int c);
void	md_raw_standout(void);
void	md_raw_standend(void);
int 	md_unlink_open_file(const char *file, FILE *inf);
int md_unlink_open_file_int(const char *file, int inf);
int		md_unlink(const char *file);
int		md_chmod(const char *filename, int mode);
void	md_normaluser(void);
uid_t	md_getuid(void);
pid_t	md_getpid(void);
char 	*md_getusername(void);
char 	*md_gethomedir(void);
void	md_sleep(int s);
char 	*md_getshell(void);
int		md_shellescape(void);
int		directory_exists(const char *dirname);
char 	*md_getrealname(uid_t uid);
char 	*md_getpass(const char *prompt);
int		md_erasechar(void);
int		md_killchar(void);
int		md_dsuspchar(void);
int 	md_setdsuspchar(int c);
int		md_suspchar(void);
int		md_setsuspchar(int c);
int 	md_readchar(WINDOW *win);
int     md_loadav(double *avg);
void	md_ignoreallsignals(void);
void	md_tstphold(void);
void	md_tstpresume(void (*tstp)(int));
void	md_tstpsignal(void);
long    md_memused(void);
int     md_ucount(void);
int     md_lockfile(FILE *fp);
int     md_unlockfile(FILE *fp);
FILE 	*md_fdopen(int fd, const char *mode);
void	md_ignore_signals();
int		md_fileno(FILE *fp);
unsigned long int md_ntohl(unsigned long int x);
unsigned long int md_htonl(unsigned long int x);
void	md_flushinp();
const char *md_getroguedir();
int		md_rand();
void	md_srand(int seed);
char 	*md_crypt(const char *key, const char *salt);
char 	*md_gethostname();
unsigned short md_ntohs(unsigned short x);
unsigned short md_htons(unsigned short x);
int md_rand_range(int range);
char *md_strdup(const char *s);
long md_random();
void md_srandom(unsigned x);

int md_exec(const char *path, const char *arg0, const char *arg1);

typedef struct EFILE {
	FILE *fp;
	unsigned int efp_cksum;
	unsigned int efp_seed;
	int efp_iomode;
	int efp_error;
} EFILE;

void efclearerr(EFILE *efp);
void efseterr(EFILE *efp, int err);
int eferror(EFILE *efp);
size_t efwriten(const void *ptr, size_t size, EFILE *efp);
size_t efreadn(void *ptr, size_t size, EFILE *efp);
size_t efread(void *ptr, size_t size, size_t nitems, EFILE *efp);
size_t efwrite(const void *ptr, size_t size, size_t nitems, EFILE *efp);
EFILE *efopen(const char *filename, const char *mode);
int efclose(EFILE *efp);

void md_exit(int code);

#endif
