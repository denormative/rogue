
#ifndef COORD_H
#define COORD_H

/*
 * Coordinate data type
 */

typedef struct {
	int x;
	int y;
} coord;

#endif
