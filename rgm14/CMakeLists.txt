set(CMAKE_CXX_FLAGS ${ROGUE_FLAGS})

include_directories(.)
file(GLOB RGM14SRC *.c)
file(GLOB RGM14INC *.h)

set_source_files_properties(
    ${RGM14SRC} ${RGM14SRC} ${VARIANTSRC_A}
    PROPERTIES LANGUAGE ${ROGUE_LANG} )

add_executable (rgm14
    ${RGM14SRC}  ${RGM14INC}
    ${PDCURSESRC}   ${PDCURSEINC}
    ${ROGUEUNIFYSRC}  ${ROGUEUNIFYINC}
    ${VARIANTSRC_A} ${VARIANTINC_A}
    )

if(CURSES_FOUND)
    target_link_libraries (rgm14 ${CURSES_LIBRARIES})
endif()
