/*
 * Rogue
 *
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include <stdlib.h>
#include <string.h>
#include "curses.h"
#include <signal.h>
#include <time.h>
#include "mach_dep.h"
#include "rogue.h"

#ifdef CHECKTIME
static int num_checks;		/* times we've gone over in checkout() */
#endif

/*
 * fruits that you get at startup
 */
static char *funfruit[] = {
	"candleberry",	"caprifig",	"dewberry",	"elderberry",
	"gooseberry",	"guanabana",	"hagberry",	"ilama",
	"imbu",		"jaboticaba",	"jujube",	"litchi",
	"mombin",	"pitanga",	"prickly pear", "rambutan",
	"sapodilla",	"soursop",	"sweetsop",	"whortleberry",
	"jellybean",	"apple",	"strawberry",	"blueberry",
	"peach",	"banana"
};
#define NFRUIT (sizeof(funfruit) / sizeof (char *))

int main(int argc, char *argv[], char **envp) {
	char *env;
	int lowtime;
	time_t now;

	md_init(0);

	/*
	 * get home and options from environment
	 */

	strncpy(home,md_gethomedir(),LINELEN);

	/* Get default save file */
	strcpy(file_name, home);
	strcat(file_name, "rogue.save");

	/* Get default score file */
	strcpy(score_file, SCOREFILE);

	if ((env = getenv("ROGUEOPTS")) != NULL)
	parse_opts(env);

	if (env == NULL || whoami[0] == '\0')
	strucpy(whoami, md_getusername(), strlen(md_getusername()));

	if (whoami[0] == '\0')
	{
	printf("Say, who the hell are you?\n");
	exit(1);
	}

	if (env == NULL || fruit[0] == '\0') {
	md_srand((md_getpid()+(int)time(0)));
	strcpy(fruit, funfruit[rnd(NFRUIT)]);
	}

	/*
	 * check for print-score option
	 */
	if (argc == 2 && strcmp(argv[1], "-s") == 0)
	{
	waswizard = TRUE;
	score(0, SCOREIT, 0);
	exit(0);
	}

#ifdef WIZARD
	/*
	 * Check to see if he is a wizard
	 */
	if (argc >= 2 && argv[1][0] == '\0')
	if (strcmp(PASSWD, md_crypt(md_getpass("Wizard's password: "), "mT")) == 0)
	{
		wizard = TRUE;
		argv++;
		argc--;
	}
#endif

	if (argc == 2)
	if (!restore(argv[1], envp)) /* Note: restore will never return */
		exit(1);
	lowtime = (int) time(&now);
	dnum = (wizard && getenv("SEED") != NULL ?
	atoi(getenv("SEED")) :
	lowtime + md_getpid());
	if (wizard)
	printf("Hello %s, welcome to dungeon #%d", whoami, dnum);
	else
	printf("Hello %s, just a moment while I dig the dungeon...", whoami);
	fflush(stdout);
	seed = dnum;
	md_srand(seed);

	init_things();			/* Set up probabilities of things */
	init_colors();			/* Set up colors of potions */
	init_stones();			/* Set up stone settings of rings */
	init_materials();			/* Set up materials of wands */
	initscr();				/* Start up cursor package */
	init_names();			/* Set up names of scrolls */
	init_misc();			/* Set up miscellaneous magic */
	if (MY_LINES < 24 || MY_COLS < 80) {
	printf("\nERROR: screen size to small for rogue\n");
	byebye(-1);
	}
	setup();
	/*
	 * Set up windows
	 */
	cw = newwin(MY_LINES, MY_COLS, 0, 0);
	mw = newwin(MY_LINES, MY_COLS, 0, 0);
	hw = newwin(MY_LINES, MY_COLS, 0, 0);
	msgw = newwin(4, MY_COLS, 0, 0);
	keypad(cw,1);
	keypad(msgw,1);

	init_player();			/* Roll up the rogue */
	waswizard = wizard;

	new_level(NORMLEV);			/* Draw current level */

	/* Choose a quest item */
	quest_item = rnd(MAXRELIC);
	msg("Your quest is to retrieve the %s....",
	rel_magic[quest_item].mi_name);

	/*
	 * Start up daemons and fuses
	 */
	start_daemon(doctor, 0, AFTER);
	fuse(swander, 0, WANDERTIME, AFTER);
	start_daemon(stomach, 0, AFTER);
	start_daemon(runners, 0, AFTER);
	if (player.t_ctype == C_THIEF)
	start_daemon(trap_look, 0, AFTER);

	mpos = 0;
	playit();
	return(0);
}

/*
 * endit:
 *	Exit the program abnormally.
 */

void endit(int sig) {
	NOOP(sig);
	fatal("Ok, if you want to exit that badly, I'll have to allow it\n");
}

/*
 * fatal:
 *	Exit the program, printing a message.
 */

void fatal(char *s) {
	clear();
	move(MY_LINES-2, 0);
	printw("%s", s);
	draw(stdscr);
	endwin();
	printf("\n");	/* So the curser doesn't stop at the end of the line */
	exit(0);
}

/*
 * rnd:
 *	Pick a very random number.
 */

int rnd(int range) {
	return(range == 0 ? 0 : md_rand() % range);
}

/*
 * roll:
 *	roll a number of dice
 */

int roll(int number, int sides) {
	int dtotal = 0;

	while(number--)
	dtotal += rnd(sides)+1;
	return dtotal;
}
# ifdef SIGTSTP
/*
 * handle stop and start signals
 */
void tstp(int sig) {
	mvcur(0, MY_COLS - 1, MY_LINES - 1, 0);
	endwin();
	fflush(stdout);
	kill(0, SIGTSTP);
	signal(SIGTSTP, tstp);
	crmode();
	noecho();
	keypad(cw,1);
	clearok(curscr, TRUE);
	touchwin(cw);
	draw(cw);
	flushinp();
}
# endif

void setup() {

#ifndef DUMP
#ifdef SIGHUP
	signal(SIGHUP, auto_save);
#endif
	signal(SIGILL, bugkill);
#ifdef SIGTRAP
	signal(SIGTRAP, bugkill);
#endif
#ifdef SIGIOT
	signal(SIGIOT, bugkill);
#endif
#ifdef SIGEMT
	signal(SIGEMT, bugkill);
#endif
	signal(SIGFPE, bugkill);
#ifdef SIGBUS
	signal(SIGBUS, bugkill);
#endif
	signal(SIGSEGV, bugkill);
#ifdef SIGSYS
	signal(SIGSYS, bugkill);
#endif
#ifdef SIGPIPE
	signal(SIGPIPE, bugkill);
#endif
	signal(SIGTERM, auto_save);
#endif

	signal(SIGINT, quit);
#ifndef DUMP
#ifdef SIGQUIT
	signal(SIGQUIT, endit);
#endif
#endif
#ifdef SIGTSTP
	signal(SIGTSTP, tstp);
#endif
	crmode();				/* Cbreak mode */
	noecho();				/* Echo off */
}

/*
 * playit:
 *	The main loop of the program.  Loop until the game is over,
 * refreshing things and looking at the proper times.
 */

void playit() {
	char *opts;


	/*
	 * parse environment declaration of options
	 */
	if ((opts = getenv("ROGUEOPTS")) != NULL)
	parse_opts(opts);


	player.t_oldpos = hero;
	oldrp = roomin(&hero);
	after = TRUE;
	while (playing)
	command();			/* Command execution */
	endit(0);
}


/*
 * author:
 *	See if a user is an author of the program
 */
int author() {
	switch (md_getuid()) {
#if AUTHOR
		case AUTHOR:
#endif
		case 0:
			return TRUE;
		default:
			return FALSE;
	}
}


/*
 * holiday:
 *	Returns TRUE when it is a good time to play rogue
 */
int holiday() {
	time_t now;
	struct tm *localtime();
	struct tm *ntime;

	time(&now);			/* get the current time */
	ntime = localtime(&now);
	if(ntime->tm_wday == 0 || ntime->tm_wday == 6)
		return TRUE;		/* OK on Sat & Sun */
	if(ntime->tm_hour < 8 || ntime->tm_hour >= 17)
		return TRUE;		/* OK before 8AM & after 5PM */
	if(ntime->tm_yday <= 7 || ntime->tm_yday >= 350)
		return TRUE;		/* OK during Christmas */
#if 0 /* not for now */
	if (access("/usr/tmp/.ryes",0) == 0)
		return TRUE;		/* if author permission */
#endif

	return FALSE;			/* All other times are bad */
}
