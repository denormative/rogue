set(CMAKE_CXX_FLAGS ${ROGUE_FLAGS})

include_directories(.)
file(GLOB XROGUESRC *.c)
file(GLOB XROGUEINC *.h)

set_source_files_properties(
    ${XROGUESRC} ${XROGUESRC} ${VARIANTSRC_A}
    PROPERTIES LANGUAGE ${ROGUE_LANG} )

add_executable (xrogue
    ${XROGUESRC}  ${XROGUEINC}
    ${PDCURSESRC}   ${PDCURSEINC}
    ${ROGUEUNIFYSRC}  ${ROGUEUNIFYINC}
    ${VARIANTSRC_A} ${VARIANTINC_A}
    )

if(CURSES_FOUND)
    target_link_libraries (xrogue ${CURSES_LIBRARIES})
endif()
