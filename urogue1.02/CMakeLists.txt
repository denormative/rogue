set(CMAKE_CXX_FLAGS ${ROGUE_FLAGS})

include_directories(.)
file(GLOB UROGUE102SRC *.c)
file(GLOB UROGUE102INC *.h)

set_source_files_properties(
    ${UROGUE102SRC} ${UROGUE102SRC} ${VARIANTSRC_A}
    PROPERTIES LANGUAGE ${ROGUE_LANG} )

add_executable (urogue102
    ${UROGUE102SRC}  ${UROGUE102INC}
    ${PDCURSESRC}   ${PDCURSEINC}
    ${ROGUEUNIFYSRC}  ${ROGUEUNIFYINC}
    ${VARIANTSRC_A} ${VARIANTINC_A}
    )

if(CURSES_FOUND)
    target_link_libraries (urogue102 ${CURSES_LIBRARIES})
endif()
