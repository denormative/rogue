# The name of our project is "HELLO". CMakeLists files in this project can
# refer to the root source directory of the project as ${HELLO_SOURCE_DIR} and
# to the root binary directory of the project as ${HELLO_BINARY_DIR}.
cmake_minimum_required (VERSION 2.6)
project (Rogue)

set(CURSES_NEED_NCURSES FALSE)
FIND_PACKAGE(Curses)

if(CURSES_FOUND)
	include_directories(CURSES_INCLUDE_DIR)
else()
	include_directories(pdcurs34)
	file(GLOB PDCURSESRC "pdcurs34/pdcurses/*.c" "pdcurs34/win32/*.c")
	file(GLOB PDCURSEINC "pdcurs34/pdcurses/*.h" "pdcurs34/win32/*.h")
endif()

include_directories(rogue_unify)
file(GLOB ROGUEUNIFYSRC "rogue_unify/*.c")
file(GLOB ROGUEUNIFYINC "rogue_unify/*.h")

#include_directories(librogue/variant)
#file(GLOB VARIANTSRC_A "librogue/variant/*-a.c")
#file(GLOB VARIANTINC_A "librogue/variant/*-a.h")
#
#file(GLOB VARIANTSRC_A1 "librogue/variant/*-a1.c")
#file(GLOB VARIANTINC_A1 "librogue/variant/*-a1.h")

if(WIN32)
	if(CMAKE_COMPILER_IS_GNUCXX)
	else()
		ADD_DEFINITIONS (/D _CRT_SECURE_NO_WARNINGS /D STDC_HEADER)
	endif()
endif()

if(APPLE)
	set(CMAKE_C_FLAGS "-m32")
	set(CMAKE_CXX_FLAGS "-m32")
endif()

# Build Options
if(CMAKE_COMPILER_IS_GNUCXX)
	set(CMAKE_C_FLAGS_DEBUG "-g -Wall -Wextra")
	set(CMAKE_C_FLAGS_RELEASE "-w -s -O3 -fexpensive-optimizations")
	set(CMAKE_CXX_FLAGS_DEBUG "-g -Wall -Wextra")
	set(CMAKE_CXX_FLAGS_RELEASE "-w -s -O3 -fexpensive-optimizations")
endif()

# Don't use:  -Wstrict-prototypes -Wold-style-definition -Wmissing-prototypes
set(MIN_FLAGS "-m32 -g -Wall -Wextra")
set(ALL_FLAGS "-m32 -g -Wall -Wextra -Wshadow -pedantic -Wpointer-arith -Wcast-qual -Wno-uninitialized -Wwrite-strings -Winit-self -Wcast-align")

set(ROGUE_FLAGS ${MIN_FLAGS})
set(ROGUE_LANG "C")
#set(ROGUE_LANG "CXX")

#add_subdirectory (rogue5.4)   #++e
#add_subdirectory (srogue)
#add_subdirectory (urogue)     #5
#add_subdirectory (urogue1.02) #++e

## variant-a
add_subdirectory (arogue5.8)  #++e
add_subdirectory (arogue5.8s) #++e
## variant-a1
add_subdirectory (arogue7.7)  #++e
add_subdirectory (xrogue)     #++e

## variant-r
#add_subdirectory (rogue3.6)   #++e
#add_subdirectory (rogue5.2)   #++e

# way too broken to fix at the moment:
#add_subdirectory (urogue1.03)

